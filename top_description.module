<?php

use Drupal\Core\Template\Attribute;

/**
 * Implements hook_preprocess_HOOK().
 */
function top_description_preprocess_fieldset(&$variables) {
  $variables['description_display'] = 'before';
}

/**
 * Implements hook_preprocess_HOOK().
 */
function top_description_preprocess_form_element(&$variables) {

  $ignore_types = [
    'radio',
    'checkbox',
  ];
  if (in_array($variables['element']['#type'], $ignore_types)) {
    // Don't move the description for these field types (it looks weird).
    return;
  }

  $variables['description_display'] = 'before';
  if ($variables['element']['#type'] == 'textarea' && empty($variables['description'])) {
    _top_description_set_text_format_description($variables);
  } // No description set to start?

}

/**
 * Implements hook_preprocess_HOOK().
 */
function top_description_preprocess_text_format_wrapper(&$variables) {
  // Remove the description from here because it will be placed above.
  $variables['description'] = '';
}

/**
 * Helper function to set descriptions for text_format fields.
 */
function _top_description_set_text_format_description(&$variables) {

  $field_name = $variables['element']['#parents'][0] ?? NULL;
  $name = $variables['element']['#name'];

  if (preg_match('/\[(.*?)\]\[\d+\]\[value\]$/', $name, $matches)) {
    // Matched field_stops[0][subform][field_text][0][value].
    $name = preg_replace('/\]\[\d+\]\[value\]$/', '', $name);
    if ($parts = explode('][', $name)) {
      $field_name = array_pop($parts);
    }
  }
  elseif (preg_match('/^(.*?)\[\d+\]\[value\]$/', $name, $matches)) {
    // Matched body[0][value].
    $field_name = $matches[1];
  }
  $bundle = $variables['element']['#bundle'] ?? NULL;
  $entity_type = $variables['element']['#entity_type'] ?? NULL;

  if ($field_name && $bundle && $entity_type) {
    $id = $entity_type . '.' . $bundle . '.' . $field_name;
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load($id);
    if ($field_config) {
      $variables['description']['content'] = $field_config->getDescription();
      $description_attributes = [];
      $description_attributes['id'] = 'edit-' . $field_name . '-' . $variables['element']['#delta'] . '-description';
      $variables['description']['attributes'] = new Attribute($description_attributes);
    } // Found field config?
  } // Can we look up the field config?

}
