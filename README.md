# Top Description

The "Description" on a form field can sometimes be lost when it's displayed at the bottom of the element, particularly on a WYSIWYG text area. See these posts:

- https://drupal.org/node/645754
- https://drupal.org/node/1600944
- https://groups.drupal.org/node/206593

This module just moves the "Description" on form elements to the top, under the label for modern (D8+) themes that support a setting like `$variables['description_display'] = 'before'`.
